import {Component} from 'react';
import styles from '../styles/Home.module.css'
import classnames from 'classnames'

class Form extends Component
{
    constructor(props) {
        super(props);

        this.state = {
            name: {
                value: '',
                validated: false,
                failed: false,
                pattern: '^.{3,29}$'
            },
            surname: {
                value: '',
                validated: false,
                failed: false,
                pattern: '^[A-Za-z0-9]{3,29}$'
            },
            age: {
                value: '',
                validated: false,
                failed: false,
                pattern: '^[1-9][8-9]|[2-9][0-9]$'
            },
            website: {
                value: '',
                validated: false,
                failed: false,
                pattern: '^(https)?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)$'
            },
            phone: {
                value: '',
                validated: false,
                failed: false,
                pattern: '^\\+\\d\\d \\d{3}-\\d{3}-\\d{3}$'
            },
        };
    }

    updateInputValue(name, value)
    {
        this.setState((prevState) => {
            const newState = {...prevState};

            newState[name].value = value;

            return newState;
        })

        console.log(this.state)
    }

    submit()
    {
        this.setState((prevState) => {
            const newState = {...prevState};

            Object.keys(newState).forEach((val) => {
                if (newState[val].value === '') {
                    newState[val].validated = true;
                    newState[val].failed = true;
                } else {
                    if (!newState[val].value.match(newState[val].pattern)) {
                        newState[val].validated = true;
                        newState[val].failed = true;
                        console.log('failed', val)
                    } else {
                        newState[val].validated = true;
                        newState[val].failed = false;
                    }
                }
            });

            return newState;
        })
    }

    isFailed(name)
    {
        return this.state[name].failed && this.state[name].validated;
    }

    isGood(name)
    {
        return !this.state[name].failed && this.state[name].validated;
    }

    render() {
        return (
            <div>
                <form className={styles.form}>
                    <div className={styles.title}>
                        This is just a simple form
                    </div>
                    <label>
                        <div className={styles.titleSmaller}>What is your name?</div>
                        <div className={styles.criteria}>
                            Length have to be greater or equal 3 and shorter than 30
                        </div>
                        {this.isFailed('name') ? (
                            <div className={styles.errors}>
                                There we have some errors
                            </div>
                        ): null}
                        {this.isGood('name') ? (
                            <div className={styles.good}>
                                Everything good
                            </div>
                        ): null}

                        <input className={classnames(styles.input, {[styles.inputFail]: this.isFailed('name'), [styles.inputGood]: this.isGood('name')})}
                               type='text' placeholder='Your name...' value={this.state.name.value}
                               onChange={(e) => this.updateInputValue('name', e.target.value)}/>
                    </label>
                    <label>
                        <div className={styles.titleSmaller}>What is your surname?</div>
                        <div className={styles.criteria}>
                            The same as with name + we accept only english characters
                        </div>
                        {this.isFailed('surname') ? (
                            <div className={styles.errors}>
                                There we have some errors
                            </div>
                        ): null}
                        {this.isGood('surname') ? (
                            <div className={styles.good}>
                                Everything good
                            </div>
                        ): null}
                        <input className={classnames(styles.input, {[styles.inputFail]: this.isFailed('surname'), [styles.inputGood]: this.isGood('surname')})}
                               type='text' placeholder='Your surname...' value={this.state.surname.value}
                               onChange={(e) => this.updateInputValue('surname', e.target.value)}/>
                    </label>
                    <label>
                        <div className={styles.titleSmaller}>What is your phone number?</div>
                        <div className={styles.criteria}>
                            Must be in format +00 000-000-000
                        </div>
                        {this.isFailed('phone') ? (
                            <div className={styles.errors}>
                                There we have some errors
                            </div>
                        ): null}
                        {this.isGood('phone') ? (
                            <div className={styles.good}>
                                Everything good
                            </div>
                        ): null}
                        <input className={classnames(styles.input, {[styles.inputFail]: this.isFailed('phone'), [styles.inputGood]: this.isGood('phone')})}
                               type='text' placeholder='Your phone number...' value={this.state.phone.value}
                               onChange={(e) => this.updateInputValue('phone', e.target.value)}/>
                    </label>
                    <label>
                        <div className={styles.titleSmaller}>What is your website address?</div>
                        <div className={styles.criteria}>
                            Must be a valid URL using https only
                        </div>
                        {this.isFailed('website') ? (
                            <div className={styles.errors}>
                                There we have some errors
                            </div>
                        ): null}
                        {this.isGood('website') ? (
                            <div className={styles.good}>
                                Everything good
                            </div>
                        ): null}
                        <input className={classnames(styles.input, {[styles.inputFail]: this.isFailed('website'), [styles.inputGood]: this.isGood('website')})}
                               type='text' placeholder='Your website address...' value={this.state.website.value}
                               onChange={(e) => this.updateInputValue('website', e.target.value)}/>
                    </label>
                    <label>
                        <div className={styles.titleSmaller}>What is your age?</div>
                        <div className={styles.criteria}>
                            We accept an age only greater or equal 18 and smaller than 100.
                        </div>
                        {this.isFailed('age') ? (
                            <div className={styles.errors}>
                                There we have some errors
                            </div>
                        ): null}
                        {this.isGood('age') ? (
                            <div className={styles.good}>
                                Everything good
                            </div>
                        ): null}
                        <input className={classnames(styles.input, {[styles.inputFail]: this.isFailed('age'), [styles.inputGood]: this.isGood('age')})}
                               type='text' placeholder='Your age...' value={this.state.age.value}
                               onChange={(e) => this.updateInputValue('age', e.target.value)}/>
                    </label>
                    <div className={styles.submit} onClick={() => this.submit()}>
                        Send form
                    </div>
                </form>
            </div>
        );
    }
}

export default Form